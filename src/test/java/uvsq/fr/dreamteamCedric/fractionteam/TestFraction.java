package uvsq.fr.dreamteamCedric.fractionteam;
import static org.junit.Assert.*;

import java.util.Scanner;
import java.util.Stack;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;


public class TestFraction {
	private Fraction f1;
	private Fraction f2;
	private int numerateur=1;
	private int denominateur=2;
	
	
	@Before
	public void initializeBeforeTest()  {
		f1= new Fraction(numerateur, denominateur);
	}
	
	@Before
	public void initializeBeforeTest1()  {
		f1= new Fraction(numerateur);
	}
	@Before
	public void initializeBeforeTest2()  {
		f1= new Fraction();
	}


	
	@Test
	public void testAddtion () {
		f1= new Fraction(numerateur,denominateur);
		f2 = new Fraction(1,4);
		Fraction f3 = Fraction.addition(f1,f2);
		assertThat(f1.getFloat()+f2.getFloat(), is(equalTo(f3.getFloat())));
				
	}
	@Test
	public void  testDouble() {
		f1= new Fraction(1,4);
		Double s = 0.25;
		assertThat(s ,is(equalTo(f1.getdoubleFraction())));
	}
	
	@Test
	public void testTostring() {
		f1= new Fraction(1,4);
		String s ="1/4";
		assertThat(s ,is(equalTo(f1.toString())));
	}
	
	
	
	

}
